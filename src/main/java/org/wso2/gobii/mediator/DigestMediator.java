package org.wso2.gobii.mediator;

import static org.junit.Assert.assertNotNull;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Stack;

import org.apache.axiom.om.OMElement;
import org.apache.axiom.soap.SOAPEnvelope;
import org.apache.axis2.AxisFault;
import org.apache.axis2.addressing.EndpointReference;
import org.apache.axis2.addressing.RelatesTo;
import org.apache.axis2.saaj.SOAPEnvelopeImpl;
import org.apache.commons.logging.Log;
import org.apache.synapse.ContinuationState;
import org.apache.synapse.FaultHandler;
import org.apache.synapse.Mediator;
import org.apache.synapse.MessageContext;
import org.apache.synapse.config.SynapseConfiguration;
import org.apache.synapse.core.SynapseEnvironment;
import org.apache.synapse.core.axis2.Axis2MessageContext;
import org.apache.synapse.endpoints.Endpoint;
import org.apache.synapse.mediators.AbstractMediator;
import com.jcraft.jsch.*;
public class DigestMediator extends AbstractMediator { 
	
public String message;

    /**
     * Test main method
     */
	public static void main(String[] args) {
		if(args.length != 6) {
			System.out.println("usage: <this> AspectName ComputeUser ComputePass InputFilePath Application(DB)Password HDFOutputLocation");
			System.out.println("    all other arguments are sane defaults.");
			System.exit(0);
		}
		MessageContext context = new org.apache.synapse.MessageContext() {
			public Object getProperty(String arg0) {
				switch(arg0) {
				case "AspectName": return args[0];
				case "ComputeUser": return args[1];
				case "ComputePass": return args[2];
				case "InputFilePath": return args[3];
				case "Application Password": return args[4];
				case "HDFOutputLocation": return args[5];
				default:return null;
				}
			}

			public SynapseConfiguration getConfiguration() {				return null;			}
			public Map<String, Object> getContextEntries() {				return null;			}
			public Stack<ContinuationState> getContinuationStateStack() {				return null;			}
			public Mediator getDefaultConfiguration(String arg0) {return null;			}
			public Endpoint getEndpoint(String arg0) {return null;			}
			public Object getEntry(String arg0) {				return null;			}
		public SynapseEnvironment getEnvironment() {				return null;}
			public Mediator getFaultSequence() {
				return null;
			}
			public Stack<FaultHandler> getFaultStack() {
				return null;
			}
			public EndpointReference getFaultTo() {
				return null;
			}
			

			@Override
			public EndpointReference getFrom() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Object getLocalEntry(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Mediator getMainSequence() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getMessageFlowTracingState() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String getMessageID() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getMessageString() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Set getPropertyKeySet() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RelatesTo getRelatesTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public EndpointReference getReplyTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Mediator getSequence(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Mediator getSequenceTemplate(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Log getServiceLog() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getSoapAction() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public EndpointReference getTo() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public int getTracingState() {
				// TODO Auto-generated method stub
				return 0;
			}

			@Override
			public String getWSAAction() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getWSAMessageID() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public boolean isContinuationEnabled() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDoingGET() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDoingMTOM() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDoingPOX() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isDoingSWA() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isFaultResponse() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isResponse() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public boolean isSOAP11() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public void pushContinuationState(ContinuationState arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void pushFaultHandler(FaultHandler arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setConfiguration(SynapseConfiguration arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setContextEntries(Map<String, Object> arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setContinuationEnabled(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setDoingGET(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setDoingMTOM(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setDoingPOX(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setDoingSWA(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnvelope(SOAPEnvelope arg0) throws AxisFault {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setEnvironment(SynapseEnvironment arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setFaultResponse(boolean arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void setFaultTo(EndpointReference arg0) {
				// TODO Auto-generated method stub
				
			}
			public void setFrom(EndpointReference arg0) {}
			public void setMessageFlowTracingState(int arg0) {			}
			public void setMessageID(String arg0) {}
			public void setProperty(String arg0, Object arg1) {}
			public void setRelatesTo(RelatesTo[] arg0) {}
			public void setReplyTo(EndpointReference arg0) {}
			public void setResponse(boolean arg0) {			}
			public void setSoapAction(String arg0) {			}
			public void setTo(EndpointReference arg0) {							}
			public void setTracingState(int arg0) {			}
			public void setWSAAction(String arg0) {}
			public void setWSAMessageID(String arg0) {			}
			public SOAPEnvelope getEnvelope() {				return null;			}
			public OMElement getFormat(String arg0) {				return null;			}
					};
					System.out.println("||||||||Mediating|||||||");
					boolean success = new DigestMediator().mediate(context);

					System.out.println("||||||||Mediation Complete|||||||");
				    System.out.println("||||||||"+success+"|||||||");
	}

	public boolean mediate(MessageContext context) { 
           //Aspect - json document describing the transform
        String digestAspect = getStringProp(context,"AspectName");
        String computeNodePath = getStringProp(context,"ComputePath");
        String computeUser = getStringProp(context,"ComputeUser");
        String computePass = getStringProp(context,"ComputePass");
        String inputFilePath = getStringProp(context,"InputFilePath");
        
        /* //These may exist in the future for element creation in the DB, or as find/replace in the Aspect.
        String contact = getStringProp(context,"Contact");
        String project = getStringProp(context,"Project");
        String experiment = getStringProp(context,"Experiment");
        String platform = getStringProp(context,"Platform");
        String dataset = getStringProp(context,"Dataset");
        String mapset = getStringProp(context,"Mapset");
         */

        //Path to HDF5 File Output Location
        String hdfOutputLocation = getStringProp(context,"HDFOutputLocation");
        //Size of HDF5 Elements, hopefully can be removed at some point
        String hdfDatatype = getStringProp(context,"HDFDatatype");
        //For database connection
        String dbURL=getStringProp(context,"Database URL");
        
		String dbUser=getStringProp(context,"Application User");
		String dbPass=getStringProp(context,"Application Password");

		//Sane Defaults for now
		if(dbURL==null)dbURL="postgres-node";
		if(dbUser==null)dbUser="appuser";
		if(computeNodePath==null)computeNodePath="compute-node";
		
		//TODO- might not eventually be a sane default
		if(hdfDatatype==null)hdfDatatype="NUCLEOTIDE_2_LETTER";
		

		//Couple sanity checks
		if(inputFilePath==null) {
			System.err.println("Failed to read input file path, expected something like /data/gobii_bundle/crop/files/file.name");
			return false;
		}
		if(dbPass==null) {
			System.err.println("Failed to read database password, expected password for supplied user in pgSQL database, defaulting to the application user");
			return false;
		}
		if(hdfOutputLocation==null) {
			System.err.println("Failed to read hdf output location, expected something like /data/gobii_bundle/crop/hdf5/DS_#.h5");
			return false;
		}
		if(computeUser==null) {
			System.err.println("Failed to read compute user, expected user for remote execution on gobii compute node");
			return false;
		}
		if(computePass==null) {
			System.err.println("Failed to read compute password, expected password for remote execution user on gobii compute node");
			return false;
		}
		if(digestAspect==null) {
			System.err.println("Failed to read an Aspect to run on data. Expected a valid json object describing the data transformations");
			return false;
		}

        
        
        //TODO - based on load type, make sure all correct info is passed, for now this is double placebo
        //LoadType loadType = getLoadType(digestAspect);
		//this should be obvious from the aspect, and right now we happily don't need to care
        
        
        boolean success= callDigest(digestAspect,inputFilePath,computeNodePath,computeUser,computePass, dbURL,dbUser,dbPass, hdfOutputLocation, hdfDatatype);
        
        return success; 
    }
	
	//Note: c.getProperty returns null if there is no prop, and (String) null returns a null string reference, so this returns a null string if no property exists in this context
	private static String getStringProp(MessageContext c, String propName) {
        return(String) c.getProperty(propName);

	}
	
	/**
	 * Calls digest process
	 * @param digestAspect String containing json-encoded aspect data
	 * @param dataFile Location of input data file
	 * @param computeNodePath Location of the gobii compute node
	 * @param databaseURL URL of the gobii database
	 * @param hdf5OutputLocation File Location of HDF5 output file (if this is a dataset load)
	 * @param hdfDataSize Size, in characters, of the maximum width of the output of the dataset (if this is a dataset load)
	 * @return If the connection to the compute node succeeded
	 */
	private static boolean callDigest(String digestAspect, String dataFile, String computeNodePath, String computeUser, String computePass, String dbURL, String dbUser, String dbPass, String hdf5OutputLocation, String hdfDataSize) {
		//TODO - with time, this can be replaced with spinning up a docker image on the compute node
				
		//Current impl - rely heavily on BERT's ability to ssh into nodes.
		//Shell shell = new Ssh(computeNodePath,computeUser,computePass);
		String execString = String.format("(cd /data/gobii_bundle/core && ./Digestor.sh -a %s -d %s -h %s -p %s -h %s -t %s)", digestAspect,dataFile,dbURL, dbUser, dbPass ,hdf5OutputLocation,hdfDataSize);
		try{
			return executeOverSSH(execString, computeNodePath, computeUser, computePass);
		}
		catch(Exception e) {
		return false; //Something went wrong, fail
		}
	}
	
	private static boolean executeOverSSH(String command, String computeNodePath, String computeUser, String computePass) throws Exception {
			JSch jsch = new JSch();
			int port = 22;
			if(computeNodePath.contains(":")){
				port = Integer.parseInt(computeNodePath.split(":")[1]);
				computeNodePath=computeNodePath.split(":")[0];
			}
			Session session=jsch.getSession(computeUser, computeNodePath, port);
			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);
			session.setPassword(computePass);
			session.connect();
			
			Channel channel=session.openChannel("exec");
			((ChannelExec)channel).setCommand(command);


			//Set to print normal output to console
			channel.setInputStream(null);
			channel.setOutputStream(System.out);
			((ChannelExec) channel).setErrStream(System.err);

			java.io.InputStream in = channel.getInputStream();

			channel.connect();

			
			//Sleep wait pump output of command
			byte[] tmp = new byte[1024];
			while (true) {
				while (in.available() > 0) {
					int i = in.read(tmp, 0, 1024);
					if (i < 0) break;
					System.out.println(" | "+new String(tmp, 0, i));
				}
				if (channel.isClosed()) {
					if (in.available() > 0) continue;
					System.out.println(" | exit-status: " + channel.getExitStatus());
					break;
				}
				try {
					Thread.sleep(1000);
				} catch (Exception e) {
				}
			}
			int status = channel.getExitStatus();
			channel.disconnect();
			session.disconnect();
			
		return status == 0;//Success on 0 status
	}
	
	private static LoadType getLoadType(String aspect) {
		//AspectObject aspect = new ObjectMapper().readValue(str, AspectObject.class)
		if(aspect.contains("\"matrix\":")) {
			return LoadType.MATRIX;
		}
		else if(aspect.contains("\"sample\":")) {
			return LoadType.SAMPLE;
		}
		else if(aspect.contains("\"marker\":")) {
			return LoadType.MARKER;
		}
		return null;
	}
    public String getMessage() {
    	return message;
    }
    public void setMessage(String messages) {
    	this.message = messages;
    }
    
}
enum LoadType{MARKER,SAMPLE,MATRIX};