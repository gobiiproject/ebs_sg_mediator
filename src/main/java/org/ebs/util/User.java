package org.ebs.util;

public class User {
    private int username;

    public User(int username) {
        this.username = username;
    }
    
    public int getUsername() {
        return username;
    }

    public void setUsername(int username) {
        this.username = username;
    }
    
}